//
//  UltimaViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class UltimaViewController: UIViewController {

    var taskDesc: String = ""
    var taskTyp: String = ""
    var pontuation: String = ""
    var worked: String = ""
    var funci: String = ""
    var inic: String = ""
    var fimn: String = ""
    
    @IBOutlet var tipo: UILabel!
    @IBOutlet var funcuinario: UILabel!
    @IBOutlet var prev: UILabel!
    @IBOutlet var gasto: UILabel!
    @IBOutlet var desc: UITextView!
    @IBOutlet var fim: UILabel!
    @IBOutlet var ini: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tipo.text = taskTyp
        desc.text = taskDesc
        prev.text = pontuation
        gasto.text = worked
        ini.text = inic
        fim.text = fimn
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
