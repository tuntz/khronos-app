//
//  ProjectCellCollectionViewCell.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ProjectCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var logo: UIImageView!
    
}
