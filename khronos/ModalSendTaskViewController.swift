//
//  ModalSendTaskViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ModalSendTaskViewController: UIViewController {
    
    @IBOutlet var SendButton: UIButton!
    @IBOutlet var obs: UITextField!
    @IBOutlet var validator: UILabel!
    @IBOutlet var viewC: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SendButton.layer.cornerRadius = 10
        SendButton.clipsToBounds = true
        
        viewC.layer.cornerRadius = 10
        viewC.clipsToBounds = true
        viewC.layer.borderWidth = 1
        viewC.layer.borderColor = UIColor.init(red:0, green:0, blue:0, alpha: 1.0).cgColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func SendTask(_ sender: UIButton) {
        if (obs.text != nil) && (obs.text?.characters.count)! > 0 {
            self.dismiss(animated: false, completion: nil)
        }
        else {
            validator.isHidden = false
        }
    }
}

extension ModalSendTaskViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
