//
//  ViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class
ViewController: UIViewController {

    @IBOutlet var enterButton: UIButton!
    @IBOutlet var user: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = " "
        enterButton.layer.cornerRadius = 10
        enterButton.clipsToBounds = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(_ sender: UIButton) {
        if (user.text != nil) && user.text == "admin" {
            performSegue(withIdentifier: "segueflux2", sender: nil)
        }
        else {
            performSegue(withIdentifier: "segueflux1", sender: nil)
        }
    }
    

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.removeBackgroundAndShadow()
//        self.navigationController?.navigationBar.preferred(tintColor: .white)
    }

}

extension UINavigationBar {
    func removeBackgroundAndShadow() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
    }
}

extension ViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
