//
//  TaskDetailViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class TaskDetailViewController: UIViewController {
    
    var taskDesc: String = ""
    var taskTyp: String = ""
    var taskPrior: String = ""
    var pontuation: String = ""
    var worked: String = ""
    var setable: Bool = false
    
    @IBOutlet var taskPriority: UILabel!
    @IBOutlet var taskType: UILabel!
    @IBOutlet var taskDescription: UITextView!
    @IBOutlet var taskWorked: UILabel!
    @IBOutlet var taskPoints: UILabel!
    @IBOutlet var setActivity: UIButton!
    @IBOutlet var finishButon: UIButton!
    @IBOutlet var historyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        taskPriority.text = taskPrior
        taskType.text = taskTyp
        taskDescription.text = taskDesc
        taskPoints.text = pontuation
        taskWorked.text = worked
        
        setActivity.isHidden = !setable
        finishButon.isHidden = setable
        
        setActivity.layer.cornerRadius = 10
        setActivity.clipsToBounds = true
        
        finishButon.layer.cornerRadius = 10
        finishButon.clipsToBounds = true
        
        historyButton.layer.cornerRadius = 10
        historyButton.clipsToBounds = true
        if setable {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(SendManualy))
            navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "ic_alarm_add")
        }
        // Do any additional setup after loading the view.
    }
    
    func SendManualy() {
        performSegue(withIdentifier: "SegueManual", sender: nil)
    }
    
    @IBAction func didGetTask(_ sender: UIButton) {
        showToast(message : "Tarefa atribuida à você")
    }
    
    
    @IBAction func sendToQA(_ sender: UIButton) {
        showToast(message : "Tarefa enviada para QA")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue4" {
            if let destinationVC = segue.destination as? HistoryViewController {
                var history: Array<String> = []
                for i in 0...25 {
                    history.append("\(i)\(i)/\(i)\(i)/\(i)\(i)  \(i)\(i):\(i)\(i) taskDesc")
                }
                destinationVC.history = history
                destinationVC.title = "Histórico " + title!
            }
        }
    }

}
