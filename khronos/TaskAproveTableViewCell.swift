//
//  TaskAproveTableViewCell.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

protocol TaskAproveTableViewCellDelegate {
    func aproved(cell: TaskAproveTableViewCell)
}

class TaskAproveTableViewCell: UITableViewCell {

    
    @IBOutlet var ate: UILabel!
    @IBOutlet var funci: UILabel!
    @IBOutlet var de: UILabel!
    
    var delegate:TaskAproveTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func aprove(_ sender: UIButton) {
        delegate?.aproved(cell: self)
    }
}
