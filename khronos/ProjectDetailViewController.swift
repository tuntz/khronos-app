//
//  ProjectDetailViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ProjectDetailViewController: UIViewController {
    
    var tasks: Array<TaskObject> = []
    var client: String = ""
    var leader: String = ""
    
    @IBOutlet var logo: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ClientName: UILabel!
    @IBOutlet var leaderLabel: UILabel!
    @IBOutlet var totalHour: UILabel!
    @IBOutlet var totalMinute: UILabel!
    @IBOutlet var filter: UISwitch!
    @IBOutlet var searchBar: UISearchBar!
    
    var imgLogo: UIImage = UIImage()
    var projName: String = ""
    
    var minutes = 0
    var hours = 0
    var timer = Timer()
    var running = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        logo.image = imgLogo
        title = projName
        ClientName.text = client
        leaderLabel.text = leader
        
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue3" {
            let indexPath = sender as! IndexPath
            if let destinationVC = segue.destination as? TaskDetailViewController {
                destinationVC.title = tasks[indexPath.row].name
                destinationVC.taskDesc = tasks[indexPath.row].description
                destinationVC.taskPrior = tasks[indexPath.row].criticality
                destinationVC.taskTyp = tasks[indexPath.row].type
                destinationVC.pontuation = String(tasks[indexPath.row].points)
                destinationVC.worked = "\(hours):\(minutes)"
                destinationVC.setable = !filter.isOn
            }
        }
    }

}

extension ProjectDetailViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Alta"
        case 1: return "Média"
        case 2: return "Baixa"
        default: return "Demais"
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            switch forSection {
            case 0:
                headerTitle.textLabel?.textColor = UIColor.red
            case 1:
                headerTitle.textLabel?.textColor = UIColor.yellow
            case 2:
                headerTitle.textLabel?.textColor = UIColor.blue
            default:
                headerTitle.textLabel?.textColor = UIColor.black
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tasks.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell",
                                                      for: indexPath) as! TaskTableViewCell
        
        cell.name.text = tasks[indexPath.row].name
        cell.delegate = self
        
        return cell
    }
}

extension ProjectDetailViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        performSegue(withIdentifier: "segue3", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 20.0
    }
    
}

extension ProjectDetailViewController: TaskTableViewCellDelegate{
    
    func didActive(cell: TaskTableViewCell){
        
        if cell.isActive && !running{
            runTimer()
        }
        else {
            performSegue(withIdentifier: "segue5", sender: nil)
            if !cell.isActive {
                pauseTimer()
            }
            else {
                pauseTimer()
                runTimer()
            }
        }
        
        let cells = tableView.visibleCells as! Array<TaskTableViewCell>
        for tbCell in cells {
            if tbCell != cell && tbCell.isActive{
                tbCell.deselect()
                tbCell.pauseTimer()
            }
        }
    }
    
    func runTimer() {
        running = true
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        minutes += 1
        if minutes == 60 {
            minutes = 0
            hours += 1
            totalHour.text = "\(hours)"
        }
        totalMinute.text = "\(minutes)"
    }
    
    func pauseTimer() {
        running = false
        timer.invalidate()
    }
    
}

extension ProjectDetailViewController: UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
}
