//
//  TaskObject.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import Foundation

class TaskObject {
    
    var name: String = ""
    var description: String = ""
    var points: Int = 0
    var criticality: String = ""
    var type: String = ""
    var attributedTo: String = ""
    var start: String = ""
    var stop: String = ""
    
}
