//
//  TaskTableViewCell.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

protocol TaskTableViewCellDelegate{
    func didActive(cell: TaskTableViewCell)
}

class TaskTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var Timing: UILabel!
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var minuteLabel: UILabel!
    
    private var hours = 0
    private var minutes = 0
    var timer = Timer()
    
    var delegate:TaskTableViewCellDelegate?
    var isActive: Bool {
        get {
            return active
        }
    }
    
    private var active = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func PlayPause(_ sender: UIButton) {
        active = !active
        if active {
            playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            runTimer()
        }
        else {
            playPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            pauseTimer()
        }
        
        delegate?.didActive(cell: self)
    }
    
    func deselect(){
        active = false
        playPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        minutes += 1
        if minutes == 60 {
            minutes = 0
            hours += 1
            hourLabel.text = "\(hours)"
        }
        minuteLabel.text = "\(minutes)"
    }
    
    func pauseTimer() {
        timer.invalidate()
    }
}
