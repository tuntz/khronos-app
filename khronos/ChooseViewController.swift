//
//  ChooseViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ChooseViewController: UIViewController {

    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Apontamento"
        
        btn1.layer.cornerRadius = 10
        btn1.clipsToBounds = true
        
        btn2.layer.cornerRadius = 10
        btn2.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSla" {
            if let destinationVC = segue.destination as? ProjectsViewController {
                destinationVC.aprov = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func choosedAdmin(_ sender: UIButton) {
        performSegue(withIdentifier: "segueSla", sender: nil)
    }
}
