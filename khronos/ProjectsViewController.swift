//
//  ProjectsViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 21/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    
    var projectArray: Array<String> = []
    
    let logoArray = [#imageLiteral(resourceName: "useravatar"), #imageLiteral(resourceName: "projectavatar1"), #imageLiteral(resourceName: "projectavatar-2"), #imageLiteral(resourceName: "noimg")]
    
    var aprov: Bool = false
    var tasks: Array<TaskObject> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0...12 {
            projectArray.append("Project \(i)")
        }
        
        for i in 0...5 {
            let task = TaskObject()
            task.name = "task-\(i)"
            task.description = "task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description task \(i) description "
            task.points = i
            task.criticality = "nivel \(i)"
            task.type = "tipo task \(i)"
            tasks.append(task)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue2" {
            let indexPath = sender as! IndexPath
            if let destinationVC = segue.destination as? ProjectDetailViewController {
                destinationVC.tasks = tasks
                destinationVC.imgLogo = logoArray[indexPath.row % 4]
                destinationVC.projName = projectArray[indexPath.row]
                destinationVC.client = "Cliente \(indexPath.row)"
                destinationVC.leader = "Lider \(indexPath.row)"
            }
        }
        else if segue.identifier == "segueAprov1" {
            let indexPath = sender as! IndexPath
            if let destinationVC = segue.destination as? ProjectDetailAprovalViewController {
                destinationVC.imgLogo = logoArray[indexPath.row % 4]
                destinationVC.projName = projectArray[indexPath.row]
                destinationVC.client = "Cliente \(indexPath.row)"
                destinationVC.leader = "Lider \(indexPath.row)"
            }
        }
    }

}

extension ProjectsViewController: UICollectionViewDataSource{
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projectArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectCellIdentifier",
                                                      for: indexPath) as! ProjectCellCollectionViewCell
        cell.logo.image = logoArray[indexPath.row % 4]
        cell.name.text = projectArray[indexPath.row]
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
}

extension ProjectsViewController: UICollectionViewDelegate{
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if !aprov {
            performSegue(withIdentifier: "segue2", sender: indexPath)
        }
        else{
            performSegue(withIdentifier: "segueAprov1", sender: indexPath)
        }
    }
}

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
