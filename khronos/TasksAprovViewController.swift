//
//  TasksAprovViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class TasksAprovViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var btn1: UIButton!
    
    var tasks: Array<TaskObject> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn1.layer.cornerRadius = 10
        btn1.clipsToBounds = true
        
        for i in 1...10 {
            let task = TaskObject()
            task.attributedTo = "func \(i)"
            task.start = "\(i)\(i):00"
            task.stop = "\(i+1)\(i+1):00"
            task.name = "task "
            task.description = "desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) desc da \(i) "
            task.type = "tipo \(i)"
            task.points = i
            tasks.append(task)
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func aproveAll(_ sender: UIButton) {
        for i in stride(from: tasks.count-1, to: -1, by: -1){
            let indexPath = IndexPath(row: i, section: 0)
            tasks.remove(at: i)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ultima" {
            let indexPath = sender as! IndexPath
            if let destinationVC = segue.destination as? UltimaViewController {
                destinationVC.title = tasks[indexPath.row].name
                destinationVC.taskDesc = tasks[indexPath.row].description
                destinationVC.taskTyp = tasks[indexPath.row].type
                destinationVC.pontuation = String(tasks[indexPath.row].points)
                destinationVC.worked = "\(indexPath.row+1)\(indexPath.row+1):00"
                destinationVC.fimn = tasks[indexPath.row].stop
                destinationVC.inic = tasks[indexPath.row].start
            }
        }
    }
}

extension TasksAprovViewController: UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tasks.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "aproveTaskCell",
                                                 for: indexPath) as! TaskAproveTableViewCell
        
        cell.funci.text = tasks[indexPath.row].attributedTo
        cell.de.text = "\(indexPath.row)\(indexPath.row):00"
        cell.ate.text = "\(indexPath.row+1)\(indexPath.row+1):00"
        cell.delegate = self
        return cell
    }
}

extension TasksAprovViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        performSegue(withIdentifier: "ultima", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension TasksAprovViewController: TaskAproveTableViewCellDelegate {
    
    func aproved(cell: TaskAproveTableViewCell){
        let indexPath = tableView.indexPath(for: cell)
        tasks.remove(at: (indexPath?.row)!)
        tableView.deleteRows(at: [indexPath!], with: .automatic)
    }
}
