//
//  ProjectDetailAprovalViewController.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class ProjectDetailAprovalViewController: UIViewController {
    
    var funcs: Array<String> = []
    var client: String = ""
    var leader: String = ""

    @IBOutlet var logo: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ClientName: UILabel!
    @IBOutlet var leaderLabel: UILabel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btn1: UIButton!
    
    var imgLogo: UIImage = UIImage()
    var projName: String = ""
    
    var minutes = 0
    var hours = 0
    var timer = Timer()
    var running = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logo.image = imgLogo
        title = projName
        ClientName.text = client
        leaderLabel.text = leader
        
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        
        btn1.layer.cornerRadius = 10
        btn1.clipsToBounds = true
        
        for i in 0...10 {
            funcs.append("func \(i)")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ProjectDetailAprovalViewController: UITableViewDataSource{
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return funcs.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "workerCell",
                                                 for: indexPath) as! WorkerTableViewCell
        
        cell.funcName.text = funcs[indexPath.row]
        cell.hoursLabel.text = "\(indexPath.row)\(indexPath.row):00 / \(indexPath.row)\(indexPath.row):00"
        return cell
    }
}

extension ProjectDetailAprovalViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        performSegue(withIdentifier: "segue10", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension ProjectDetailAprovalViewController: UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
}
