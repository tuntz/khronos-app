//
//  WorkerTableViewCell.swift
//  khronos
//
//  Created by Victor Martins Rabelo on 22/10/17.
//  Copyright © 2017 Victor Martins Rabelo. All rights reserved.
//

import UIKit

class WorkerTableViewCell: UITableViewCell {

    @IBOutlet var funcName: UILabel!
    @IBOutlet var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
